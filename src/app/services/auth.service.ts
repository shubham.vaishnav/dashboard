import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean = false;
  errMsg: string
  constructor(private _firebaseAuth: AngularFireAuth, private _router: Router, private _toastService: ToastrService) { }

  initAuthListner(){
    this._firebaseAuth.authState.subscribe( user =>{
      if(user){
        this.isLoggedIn = true;
        this._router.navigate(['/home'])
      }else{
        this.isLoggedIn = false;
        this._router.navigate(['/auth'])
      }
    })
  }

  signIn(email: string, password: string) {
    this._firebaseAuth.signInWithEmailAndPassword(email, password)
      .then(res => {
        localStorage.setItem('user', JSON.stringify(res.user))
        this._toastService.success('', 'login successfull')
      }).catch(err => {
        this.errMsg = err.message
      })
  }

  signUp(email: string, password: string) {
    this._firebaseAuth.createUserWithEmailAndPassword(email, password)
      .then(res => {
        localStorage.setItem('user', JSON.stringify(res.user))
        this._toastService.success('', 'Registration Successfull')
      }).catch(err => {
        this.errMsg = err.message
      })
  }

  logout() {
    this._firebaseAuth.signOut()
    localStorage.removeItem('user')
  }
}
