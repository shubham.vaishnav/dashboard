import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Products } from '../modals/products';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
products = new Subject<Products[]>();
  constructor(private _db:AngularFirestore, private _snackBar: MatSnackBar) { }

  async addProduct(product:Products){
   await this._db.collection('products').add(product)
    let action = 'successfully'
    let productName = product.name+' '+'Added'
    this._snackBar.open(productName, action, {
      duration: 6000,
    });
  }

  // fetchProduct(){
  //   this._db.collection('products').valueChanges().subscribe((res:Products[]) => {
  //     this.products.next(res)
  //   })
  // }
  fetchProduct(){
    return this._db.collection('products').snapshotChanges().pipe(
      map( action => {
        return action.map(a => {
          const data = a.payload.doc.data() as Products;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    )
  }

  updateProduct(productId,product:Products){
    this._db.doc('products/'+productId).update(product);
  }

  deleteProduct(productId){
    this._db.doc('products/'+productId).delete();
  }
}
