import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  product: any = {};
  constructor(public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private _fireStore: FirestoreService) { }

  ngOnInit(): void {
    this.product = this.data.product;
  }

  onDelete(productId){
    this._fireStore.deleteProduct(productId);
    this.onNoClick();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
