import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product: any = {};
  constructor(public dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private _fireStore: FirestoreService) { }

  ngOnInit(): void {
    this.product = this.data.product;
  }
  onUpdate(productID, product) {
    this._fireStore.updateProduct(productID, product);
    this.onNoClick()
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
