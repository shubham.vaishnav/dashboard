import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ErrorComponent } from './error/error.component';
import { AuthGardGuard } from './auth/guards/auth-gard.guard';


const routes: Routes = [
  {path:'',loadChildren: './dashboard/dashboard.module#DashboardModule'},
  { path: 'auth', loadChildren: () => AuthModule },
  {path:'**',component:ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
