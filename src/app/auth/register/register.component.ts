import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  errorMessage: string;
  constructor(private firebaseAuth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  register(email, password) {
    this.firebaseAuth.signUp(email, password);
    if (this.firebaseAuth.isLoggedIn) {
      // this.router.navigate(['/home'])
    }
    else {
      this.errorMessage = this.firebaseAuth.errMsg
    }
  }
}
