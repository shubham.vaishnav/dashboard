import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGardGuard implements CanActivate {

  constructor(private _auth:AuthService,private _router: Router){

  }

  canActivate(): boolean {
    if (localStorage.getItem('user')) {
      return true
    } else {
      this._router.navigate(['/auth'])
      return false
    }
  }
  
}
