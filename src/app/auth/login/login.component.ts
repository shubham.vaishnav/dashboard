import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  isDisabled:boolean = false
  loginData = {
    email: "",
    password: ""
  }
  constructor(private firebaseAuth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onLogin() {
    this.isDisabled = true
    this.firebaseAuth.signIn(this.loginData.email, this.loginData.password);
    if (localStorage.getItem('user')) {
      this.isDisabled = false
    } else {
      this.errorMessage = this.firebaseAuth.errMsg
      this.isDisabled = false
    }
  }

}
