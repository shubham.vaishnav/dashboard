import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  @Input() isDrawerOpen;
  @Input() isDrawerMode;
  @Output() closeDrawerFromSide = new EventEmitter();
  panelOpenState;
  
  ngOnInit(): void {
  }
  closeDrawer() {
    this.closeDrawerFromSide.emit()
  }
}
