import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
  @Input() isDrawer;
  @Output() openDrawrFromTop = new EventEmitter()
  isLoggedIn: boolean = false
  userName:string
  user:any
  constructor(private _authService: AuthService) {
  }

  ngOnInit(): void {
    if(localStorage.getItem('user')){
      this.isLoggedIn  = true
    }
    this.user = JSON.parse(localStorage.getItem('user'))
    this.userName = this.user.email
  }
  openDrawer() {
    this.openDrawrFromTop.emit()
  }
  Logout(){
    this._authService.logout()
  }
}
