import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Products } from 'src/app/modals/products';
import { FirestoreService } from 'src/app/services/firestore.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  products:any = {};
  constructor(private _title: Title, private _fireStore: FirestoreService) { }

  ngOnInit(): void {
    this._title.setTitle('Dashboard | Home');
  }

  onProductAdd(newProduct:Products) {
    this._fireStore.addProduct(newProduct)
  }
}
