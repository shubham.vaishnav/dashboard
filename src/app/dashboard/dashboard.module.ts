import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SideNavComponent } from './navigation/side-nav/side-nav.component';
import { TopNavComponent } from './navigation/top-nav/top-nav.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ReportsComponent } from './reports/reports.component';
// import { ToastrModule } from "ngx-toastr";
@NgModule({
  declarations: [
    DashboardComponent,
    SideNavComponent,
    TopNavComponent,
    HomeComponent,
    ProductsComponent,
    ReportsComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    MaterialModule,
    // ToastrModule.forRoot()
  ]
})
export class DashboardModule { }
