import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { ReportsComponent } from './reports/reports.component';
import { ProductsComponent } from './products/products.component';
import { AuthGardGuard } from '../auth/guards/auth-gard.guard';


const routes: Routes = [
  {path:'',component:DashboardComponent,
   children: [
    {path: '', redirectTo: 'home',pathMatch:'full'},
    {path: 'home', component: HomeComponent,canActivate:[AuthGardGuard]},
    {path: 'products', component: ProductsComponent,canActivate:[AuthGardGuard]},
    {path: 'reports', component: ReportsComponent,canActivate:[AuthGardGuard]},
  ]
  
},
{path: 'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
// [
//   {path:'',redirectTo:'home',pathMatch:'full'},
//   {pathMatch:'home',component:HomeComponent},
//   {pathMatch:'products',component:ProductsComponent},
//   {pathMatch:'reports',component:ReportsComponent}
//     ]