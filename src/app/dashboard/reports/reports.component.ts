import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Chart } from "chart.js";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  chart = [];
  constructor(private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Dashboard | Reports');
    this.reports()
  }

  reports() {
    this.chart = new Chart('canvas1', {
      type: 'line',
      data: {
        labels: ['sun', 'mon', 'tue', 'wed', 'thr', 'fri', 'sat'],
        datasets: [
          {
            label: 'Store1',
            data: [1, 14, 23, 29, 36, 45, 56, 42, 28, 19],
            borderColor: '#ff6384',
            fill: false
          },
          {
            label: 'Store2',
            data: [1, 14, 23, 29, 36, 45, 56, 42, 28, 19].reverse(),
            borderColor: '#4bc0c0',
            fill: false
          }
        ]

      }
    })
    this.chart = new Chart('canvas2', {
      type: 'bar',
      data: {
        labels: ['sun', 'mon', 'tue', 'wed', 'thr', 'fri', 'sat'],
        datasets: [
          {
            label: 'Store1',
            barPercentage: 0.5,
            barThickness: 6,
            maxBarThickness: 8,
            minBarLength: 2,
            data: [10, 20, 30, 40, 50, 60, 70],
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            borderColor: 'rgba(0, 0, 0, 0.1)',
          },
          {
            label: 'Store2',
            barPercentage: 0.5,
            barThickness: 6,
            maxBarThickness: 8,
            minBarLength: 2,
            data: [8, 18, 28, 38, 48, 58, 68],
            backgroundColor: '#ff6384',
            borderColor: '#ff6384',
          },
          {
            label: 'Store3',
            barPercentage: 0.5,
            barThickness: 6,
            maxBarThickness: 8,
            minBarLength: 2,
            data: [6, 16, 26, 36, 46, 56, 66],
            backgroundColor: '#4bc0c0',
            borderColor: '#4bc0c0',
          }
        ]

      }
    })
    this.chart = new Chart('canvas3', {
      type: 'pie',
      data: {
        datasets: [{
          data: [10, 20, 30],
          backgroundColor: ['#ffe0e6', '#dbf2f2', '#ebe0ff'],
          borderColor: ['#ffe0e6', '#dbf2f2', '#ebe0ff']
        }],
        labels: [
          'store1',
          'store2',
          'store3'
        ],
      }
    })
    this.chart = new Chart('canvas4', {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [10, 20, 30],
          backgroundColor: ['#ffe0e6', '#dbf2f2', '#ebe0ff'],
          borderColor: ['#ffe0e6', '#dbf2f2', '#ebe0ff']
        }],
        labels: [
          'store1',
          'store2',
          'store3'
        ]
      }
    })
  }

}
