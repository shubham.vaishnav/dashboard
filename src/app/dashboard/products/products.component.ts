import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Products } from 'src/app/modals/products';
import { MatSort } from '@angular/material/sort';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditProductComponent } from 'src/app/dialog/edit-product/edit-product.component';
import { map } from 'rxjs/operators';
import { ConfirmComponent } from 'src/app/dialog/confirm/confirm.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, AfterViewInit, OnDestroy {
  exchangedSubscription: Subscription;
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['name', 'price', 'quantity', 'discount', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  products: any
  constructor(private _title: Title, private _fireStore: FirestoreService,
    private _dialog: MatDialog) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this._title.setTitle('Dashboard | Products');
    // this.exchangedSubscription = this._fireStore.products.subscribe((products: Products[]) => {
    //   this.dataSource.data = products
    // })
    // this._fireStore.fetchProduct()
    this._fireStore.fetchProduct().subscribe(data => {
      this.dataSource.data = data
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openEditDialog(product): void {
    const dialogRef = this._dialog.open(EditProductComponent, {
      width: '500px',
      maxWidth: "100%",
      data: {
        product: product,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert('The dialog was closed');
    });
  }

  onDelete(product) {
    const dialogRef = this._dialog.open(ConfirmComponent, {
      width: '300px',
      maxWidth: "100%",
      data: {
        product: product,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert('The dialog was closed');
    });
  }


  ngOnDestroy() {
    // this.exchangedSubscription.unsubscribe()
  }
}
