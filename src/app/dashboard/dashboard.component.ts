import { Component, OnInit, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../services/auth.service';
import { ToastrService  } from "ngx-toastr";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dwawerMode = 'side';
  isDrawerOpen: boolean = true;
  panelOpenState = false;
  isDrawer: boolean = true;
  drawer;
  public innerWidth: any;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 769) {
      this.isDrawerOpen = false;
      this.isDrawer = false;
      this.dwawerMode = 'over';
    } else {
      this.isDrawerOpen = true;
      this.isDrawer = true;
      this.dwawerMode = 'side';
    }
  }
  constructor(private title: Title, private _authService: AuthService,private toastService:ToastrService) { }

  ngOnInit(): void {
    this.title.setTitle('Dashboard');
  }
  openDrawer() {
    this.isDrawerOpen = true;
    this.isDrawer = true
  }
  closeDrawer() {
    this.isDrawerOpen = false;
    this.isDrawer = false
  }
}
