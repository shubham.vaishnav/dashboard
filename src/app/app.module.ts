import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { AuthModule } from './auth/auth.module';
import { FormsModule } from '@angular/forms';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedmoduleModule } from './sharedmodule/sharedmodule.module';
import { AngularFireModule } from "@angular/fire";
import { environment } from 'src/environments/environment';
import { firebase } from 'src/environments/firebase';
import { AuthService } from './services/auth.service';
import { ErrorComponent } from './error/error.component';
import { ToastrModule } from "ngx-toastr";
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { FirestoreService } from './services/firestore.service';
import { EditProductComponent } from './dialog/edit-product/edit-product.component';
import { ConfirmComponent } from './dialog/confirm/confirm.component';
@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    EditProductComponent,
    ConfirmComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    AuthModule,
    FormsModule,
    DashboardModule,
    ToastrModule.forRoot(),
    SharedmoduleModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebase.firebase)
  ],
  providers: [AuthService,FirestoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
