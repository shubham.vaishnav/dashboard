export interface Products {
  id?: any;
  name: string;
  price: number;
  quantity: number;
  discount: number;
  action?: any;
}
